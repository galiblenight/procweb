from django.conf.urls import patterns, include, url
from django.contrib import admin

urlpatterns = patterns('',
    # Examples:
    url(r'^code/$', 'procapps.views.code_page', name='code'),
    url(r'^regis/$', 'procapps.views.signup', name='register'),
    url(r'^logout/$', 'procapps.views.logout', name='logout'),
    url(r'^$', 'procapps.views.home', name='home'),
    url(r'^select/$', 'procapps.views.select', name='select'),
    # url(r'^blog/', include('blog.urls')),

    # url(r'^admin/', include(admin.site.urls)),
)
