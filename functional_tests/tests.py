from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import unittest, time

class NewVisitorTest(LiveServerTestCase):
    

    def setUp(self):
        self.browser = webdriver.Firefox()
        self.browser.implicitly_wait(3)

    def tearDown(self):
        self.browser.quit()

    def test_django_processing_web_page_set_up_correct_and_can_register(self):
        testcode = ["void setup(){\n  size(500, 500);\n  background(255);\n  fill(0, 255, 0);\n}\n\nvoid draw(){\n  ellipse(width/2 + cos(frameCount/20)*width/3  + cos(frameCount/4)*width/10, height/2 + sin(frameCount/20)*width/3 + sin(frameCount/4)*width/10, 50, 50);\n}", "void setup(){\n  size(500, 500);\n  fill(255, 0, 0);\n} \n\nvoid draw(){\n  background(255);\n  ellipse(width/2, height/2, 100 + sin(frameCount/20)*50, 100 + sin(frameCount/20)*50);\n}"]
        self.browser.get(self.live_server_url)

        ###homepage check###
        self.assertIn('Welcome to Processing web', self.browser.title)
        time.sleep(6)
        self.browser.find_element_by_id('b').click()

        ###register and signin page check###
        self.assertIn('***registeration***', self.browser.title)
        self.assertIn('username', self.browser.find_element_by_id('userin').get_attribute('placeholder'))
        self.assertIn('username', self.browser.find_element_by_id('userup').get_attribute('placeholder'))
        self.assertIn('password', self.browser.find_element_by_id('passin').get_attribute('placeholder'))
        self.assertIn('password', self.browser.find_element_by_id('passup').get_attribute('placeholder'))
        self.assertIn('nickname', self.browser.find_element_by_id('nickname').get_attribute('placeholder'))
        self.assertIn('your name', self.browser.find_element_by_id('name').get_attribute('placeholder'))
        self.assertIn('lastname', self.browser.find_element_by_id('lastname').get_attribute('placeholder'))
        self.assertIn('your birthday', self.browser.find_element_by_id('birth').get_attribute('placeholder'))

        ###registeration###
        self.browser.find_element_by_id('signup').click()
        time.sleep(1)
 
        self.browser.find_element_by_id('userup').send_keys('galible')
        time.sleep(1)
        self.browser.find_element_by_id('signup').click()

        self.browser.find_element_by_id('passup').send_keys('1234567890')
        time.sleep(1)
        self.browser.find_element_by_id('signup').click()

        self.browser.find_element_by_id('nickname').send_keys('top')
        time.sleep(1)
        self.browser.find_element_by_id('signup').click()

        self.browser.find_element_by_id('name').send_keys('tanawat')
        time.sleep(1)
        self.browser.find_element_by_id('signup').click()

        self.browser.find_element_by_id('lastname').send_keys('horsirimanon')
        time.sleep(1)
        self.browser.find_element_by_id('signup').click()

        self.browser.find_element_by_id('birth').send_keys('21/02/38')
        time.sleep(1)
        self.browser.find_element_by_id('signup').click()

        ###Check register success and logout###
        time.sleep(2)
        self.assertIn('Procweb', self.browser.title)
        self.browser.find_element_by_id('logout').click()
        self.browser.get(self.live_server_url+'/code')
        self.assertIn('You are not register !!', self.browser.title)
        time.sleep(2)
        
        ###signin and check###
        self.browser.get(self.live_server_url+'/regis')
        time.sleep(2)

        self.browser.find_element_by_id('signin').click()
        time.sleep(1)
 
        self.browser.find_element_by_id('userin').send_keys('galible')
        time.sleep(1)
        self.browser.find_element_by_id('signin').click()

        self.browser.find_element_by_id('passin').send_keys('1234567890')
        time.sleep(1)
        self.browser.find_element_by_id('signin').click()
        time.sleep(2)

        ###test render project###
        self.assertIn('Procweb', self.browser.title)

        self.browser.find_element_by_id('text_code').send_keys(testcode[0])
        time.sleep(1)
        self.browser.find_element_by_id('render').click()
        self.browser.find_element_by_id('renderproc').click()
        time.sleep(5)
        self.browser.find_element_by_id('save').click()
        time.sleep(1)


        self.browser.find_element_by_id('project').send_keys('bubble ellipse')
        time.sleep(1)
        self.browser.find_element_by_id('save').click()
        time.sleep(1)
        self.browser.find_element_by_id('disc').send_keys('fun bubble ellipse ha ha ha++')
        time.sleep(1)
        self.browser.find_element_by_id('save').click()
        time.sleep(1)
        self.browser.find_element_by_id('confirm').click()
        time.sleep(1)
        self.browser.find_element_by_id('clear').click()
        self.browser.find_element_by_id('project').send_keys('confuse ball')
        self.browser.find_element_by_id('disc').send_keys('the ball that you must confuse with it')
        self.browser.find_element_by_id('text_code').send_keys(testcode[1])
        time.sleep(1)
        self.browser.find_element_by_id('save').click()
        time.sleep(1)
        self.browser.find_element_by_id('renderproc').click()
        time.sleep(1)
        self.browser.find_element_by_id('sel').click()
        time.sleep(1)
        self.browser.find_element_by_id('sel_1').click()
        time.sleep(1)

        ###test data correct###
        self.assertTrue(self.browser.find_element_by_id('text_code').text.replace('\n', '').replace(' ', ''), testcode[0].replace('\n', '').replace(' ', ''))
        self.assertTrue(self.browser.find_element_by_id('project').get_attribute('value'), 'bubble ellipse')
        self.assertTrue(self.browser.find_element_by_id('disc').text, 'fun bubble ellipse ha ha ha++')

        self.browser.find_element_by_id('sel').click()
        time.sleep(1)
        self.browser.find_element_by_id('sel_2').click()
        time.sleep(1)

        self.assertTrue(self.browser.find_element_by_id('text_code').text.replace('\n', '').replace(' ', ''), testcode[1].replace('\n', '').replace(' ', ''))
        self.assertTrue(self.browser.find_element_by_id('project').get_attribute('value'), 'confuse ball')
        self.assertTrue(self.browser.find_element_by_id('disc').text, 'the ball that you must confuse with it')

        self.browser.find_element_by_id('sel').click()
        time.sleep(1)
        self.browser.find_element_by_id('del_2').click()
        time.sleep(2)

        self.browser.find_element_by_id('b').click()
        time.sleep(2)

        self.browser.find_element_by_id('logout').click()
        time.sleep(1)

        self.browser.find_element_by_id('b').click()
        time.sleep(1)

        ###register another user###
        self.browser.find_element_by_id('userup').send_keys('jam_eiei')
        self.browser.find_element_by_id('passup').send_keys('0987654321')
        self.browser.find_element_by_id('nickname').send_keys('jam')
        self.browser.find_element_by_id('name').send_keys('areeya')
        self.browser.find_element_by_id('lastname').send_keys('rattichunnahachot')
        self.browser.find_element_by_id('birth').send_keys('06/02/38')
        time.sleep(2)
        self.browser.find_element_by_id('signup').click()
        time.sleep(2)
        self.browser.find_element_by_id('sel').click()
        time.sleep(1)
        self.browser.find_element_by_id('b').click()
        time.sleep(2)


if __name__ == '__main__':
    unittest.main(warnings='ignore')
