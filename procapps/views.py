from django.shortcuts import redirect, render
from django.http import HttpResponse
from procapps.models import data, user
import time

def code_page(request):
    user_text = selectuser = project_text = ops = rendercode = ''
    if request.session.get('user_session', '') == '':
        return render(request, 'nomember.html')
    if request.method == 'GET':
        if request.GET.get('project', '') != '':
            item = data.objects.get(user=request.session['user_session'],project=request.GET['project'])
            return render(request, 'code.html', {
                'data' : item,
                'rendercode' : item.code,
                'ops' : 'successload',
                'user' : request.session['user_session'],
            })
    elif request.method == 'POST':
        if request.POST.get('logout','') == 'logout':
            request.session['user_session'] = ''
            return redirect('/logout')
        user_text = request.session['user_session']
        project_text = request.POST.get('project', '')
        code_text = request.POST.get('text_code', '')
        discript_text = request.POST.get('discription', '')
        if request.POST.get('ops','') == 'confirmsave':
            item = data.objects.get(user=user_text,project=project_text)
            item.discription = discript_text
            item.code = code_text
            item.save()
            rendercode = code_text
            ops = 'savechange'
        elif request.POST.get('ops','') == 'save':
            try:
                if project_text == '':
                   ops = 'projectfail'
                test_proj = data.objects.get(user=user_text,project=project_text)
                ops = 'existed'
            except:
                if ops == '':
                    data.objects.create(user=user_text,project=project_text,discription=discript_text,code=code_text)
                    ops = 'success'
        elif request.POST.get('ops','') == 'render':
            ops = 'render'
            rendercode = code_text
        elif request.POST.get('ops','') == 'clear':
            project_text = discript_text = code_text = ''
        elif request.POST.get('select','') == 'select':
            return redirect('/select')
        if ops != 'success' and ops != 'savechange':
            return render(request, 'code.html', {
                'code' : code_text,
                'rendercode' : rendercode,
                'discript' : discript_text,
                'project' : project_text,
                'ops' : ops,
                'user' : request.session['user_session'],
            })
        item = data.objects.get(user=user_text,project=project_text)
        return render(request, 'code.html', {
            'data' : item,
            'rendercode' : item.code,
            'ops' : ops,
            'user' : request.session['user_session'],
        })
    return render(request, 'code.html', {
            'project' : project_text,
            'user' : request.session['user_session'],
        })


#############################################################

def signup(request):
    if not request.session.get('user_session','') == '':
        return redirect('/code')
    if request.method == 'POST':
        tokens = ['', '', '', '', '', '', '', '']
        resume = ['userin', 'passin', 'userup', 'passup', 'nickname', 'name', 'lastname', 'birth']
        for i in range(8):
            tokens[i] = request.POST.get(resume[i],'')
        if request.POST.get('signup','') == 'true':
            for i in range(6):
                if tokens[i+2] == '':
                    return render(request, 'regis.html', {
                        'fail' : resume[i+2],
                        'catch_userup' : tokens[2],
                        'catch_passup' : tokens[3],
                        'catch_nickname' : tokens[4],
                        'catch_name' : tokens[5],
                        'catch_lastname' : tokens[6],
                        'catch_birth' : tokens[7],
                    })
            try:
                username = user.objects.get(user=tokens[2])
            except:
                user.objects.create(
                    user=tokens[2],
                    pw=tokens[3],
                    nickname=tokens[4],
                    name=tokens[5],
                    lastname=tokens[6],
                    birth=tokens[7],
                )
                request.session['user_session'] = tokens[2]
                return redirect('/code')
            return render(request, 'regis.html', {
                 'fail' : 'sameuser',
                'catch_passup' : tokens[3],
                'catch_nickname' : tokens[4],
                'catch_name' : tokens[5],
                'catch_lastname' : tokens[6],
                'catch_birth' : tokens[7],
            })
        if request.POST.get('signin','') == 'true':
            for i in range(2):
                if tokens[i] == '':
                    return render(request, 'regis.html', {
                        'fail' : resume[i],
                        'catch_userin' : tokens[0],
                        'catch_passin' : tokens[1],
                    })
            try:
                item = user.objects.get(user=tokens[0])
            except:
                return HttpResponse('<html><head><title>no user match</title><h1>no user match</h1><br /></head>no user match with user that you input please enter correct user<br /><a href="/regis">go back</a></html>')
            if item.pw == tokens[1]:
                request.session['user_session'] = tokens[0]
            else: return HttpResponse('<html><head><title>password not match</title><h1>password not match</h1><br /></head>User and password not match<br /><a href="/regis">go back</a></html>')
            return redirect('/code')

    return render(request, 'regis.html')



##############################################################

def logout(request):
    return render(request, 'logout.html')

##############################################################

def home(request):
    return render(request, 'home.html')

##############################################################

def select(request):
    if request.session.get('user_session','') == '':
        return redirect('/code')
    if request.method == 'POST':
        ops = request.POST['ops'].split('_')
        if ops[0] == 's':
            return redirect('/code?project='+data.objects.get(id=int(ops[1])).project)
        elif ops[0] == 'd':
            data.objects.get(id=int(ops[1])).delete()
        return redirect('/select')
    item = data.objects.filter(user=request.session['user_session'])
    return render(request, 'selectproj.html', {
            'projects':item,
        })
