# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('procapps', '0005_data_pw'),
    ]

    operations = [
        migrations.DeleteModel(
            name='project',
        ),
        migrations.RenameField(
            model_name='data',
            old_name='pw',
            new_name='code',
        ),
    ]
