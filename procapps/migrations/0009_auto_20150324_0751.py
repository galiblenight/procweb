# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('procapps', '0008_auto_20150324_0728'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='lastname',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='user',
            name='nickname',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
