# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('procapps', '0002_auto_20150322_1133'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Item',
            new_name='data',
        ),
    ]
