# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('procapps', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='item',
            name='code',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='item',
            name='user',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
