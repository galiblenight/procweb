# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('procapps', '0010_test'),
    ]

    operations = [
        migrations.DeleteModel(
            name='test',
        ),
    ]
