# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('procapps', '0004_auto_20150323_0401'),
    ]

    operations = [
        migrations.AddField(
            model_name='data',
            name='pw',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
