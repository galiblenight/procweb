# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('procapps', '0007_auto_20150323_0808'),
    ]

    operations = [
        migrations.CreateModel(
            name='user',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('user', models.TextField(default='')),
                ('pw', models.TextField(default='')),
                ('name', models.TextField(default='')),
                ('birth', models.TextField(default='')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.DeleteModel(
            name='project',
        ),
        migrations.RenameField(
            model_name='data',
            old_name='pw',
            new_name='code',
        ),
        migrations.AddField(
            model_name='data',
            name='discription',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='data',
            name='project',
            field=models.TextField(default=''),
            preserve_default=True,
        ),
    ]
