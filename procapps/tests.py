from django.test import TestCase
from django.template.loader import render_to_string
from procapps.models import data
from django.core.urlresolvers import resolve
from procapps.views import home_page 
from django.http import HttpRequest

class HomePageTest(TestCase):

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')  
        self.assertEqual(found.func, home_page)  

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = home_page(request)
        expected_html = render_to_string('home.html')
        self.assertEqual(response.content.decode(), expected_html)


class ItemModelTest(TestCase):

    def test_user_and_code_is_correct(self):
        first_item = data()
        first_item.user = 'first item user'
        first_item.code = 'first item code'
        first_item.save()

        second_item = data()
        second_item.user = 'second item user'
        second_item.code = 'second item code'
        second_item.save()

        saved_items = data.objects.all()
        self.assertEqual(saved_items.count(), 2)

        first_saved_item = saved_items[0]
        second_saved_item = saved_items[1]
        self.assertEqual(first_saved_item.user, 'first item user')
        self.assertEqual(first_saved_item.code, 'first item code')
        self.assertEqual(second_saved_item.user, 'second item user')
        self.assertEqual(second_saved_item.code, 'second item code')
